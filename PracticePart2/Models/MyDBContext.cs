﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Data.Entity;

namespace PracticePart1.Models
{
    public class MyDBContext : DbContext
    {
        public DbSet<CustomerDTO> Customers
        {
            get;
            set;
        }
        public DbSet<ProductDTO> Product
        {
            get;
            set;
        }

        public DbSet<StockDTO> Stock
        {
            get;
            set;
        }
        public DbSet<UserDTO> User
        {
            get;
            set;
        }
        public DbSet<InvoiceDTO> Invoice
        {
            get;
            set;
        }
        public MyDBContext()
        {
            this.Configuration.ProxyCreationEnabled = false;
        }
    }
}