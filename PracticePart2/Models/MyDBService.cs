﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PracticePart1.Models
{
    public class MyDBService
    {
        /*
            using (var db = new MyDBContext())
            {
                //Execute dbo.SaveCustomer 1,'Bilal','Lahore'
                var query = String.Format("Execute dbo.SaveCustomer {0},'{1}','{2}'",
                                    dto.CustomerID,dto.Name,dto.Address);       
                var result = db.Database.SqlQuery<Int32>(query).FirstOrDefault();
                return result;
            }
        }*/
        public int SaveCustomer(CustomerDTO dto)
        {
            using (var dbCtx = new MyDBContext())
            {
                dbCtx.Customers.Add(dto); //Add object to save

                dbCtx.SaveChanges(); //Save in database

                return dto.CustomerID; //return the auto generated customer id
            }
        }
        public int SaveProduct(ProductDTO dto)
        {
            using (var dbCtx = new MyDBContext())
            {
                dbCtx.Product.Add(dto); //Add object to save

                dbCtx.SaveChanges(); //Save in database

                return dto.ProductID; //return the auto generated customer id
            }
        }

        public int SaveStock(StockDTO dto)
        {
            using (var dbCtx = new MyDBContext())
            {
                dbCtx.Stock.Add(dto); //Add object to save

                dbCtx.SaveChanges(); //Save in database

                return dto.ProductID; //return the auto generated customer id
            }
        }
        public List<CustomerDTO> GetAllCustomers()
        {
            
                using (var dbCtx = new MyDBContext())
                {
                    return dbCtx.Customers.ToList();
                }
            
            
        }

        public List<string> Search (string name)
        {

            MyDBContext ctx= new MyDBContext();
            return ctx.Product.Where(p => p.Name.StartsWith(name)).Select(p=> p.Name).ToList();

        }
    }
}