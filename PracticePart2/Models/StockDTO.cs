﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PracticePart1.Models
{
    [Table("Stock")]
    public class StockDTO
    {
        [Key]
        public int StockID { get; set; }
        public int ProductID { get; set; }
        public int Quantity { get; set; }
        public String LastUpdated { get; set; }
       
    }
}