﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PracticePart1.Models
{
    public class CustomerRepository
    {
       /* public int SaveCustomerBySP(CustomerDTO dto)
        {
            MyDBService dataService = new MyDBService();
            int cid = dataService.SaveCustomerBySP(dto);
            return cid;
        }*/
        public int SaveCustomer(CustomerDTO dto)
        {
            MyDBService dataService = new MyDBService();
            int cid = dataService.SaveCustomer(dto);
            return cid;
        }
        public List<CustomerDTO> GetAllCustomers()
        {
            MyDBService dataService = new MyDBService();
            List<CustomerDTO> result = dataService.GetAllCustomers();
            return result;
        }
    }
}