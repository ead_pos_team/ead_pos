﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PracticePart1.Models
{
    [Table("User")]
    public class UserDTO
    {
        [Key]
        [Required(ErrorMessage = "Please Provide Username", AllowEmptyStrings = false)]
        public String Email { get; set; }
        [Required(ErrorMessage = "Please Provide Password", AllowEmptyStrings = false)]
        public String Password { get; set; }

    }
}