﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PracticePart1.Models
{
    [Table("Customers")]
    public class CustomerDTO
    {
        [Key]
        public int CustomerID { get; set; }
        public String Name { get; set; }
        public String Address { get; set; }
        public int PhoneNumber { get; set; }
    }
}