﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PracticePart1.Models
{
    [Table("Invoice")]
    public class InvoiceDTO
    {
        [Key]
        public int InvoiceID { get; set; }
        public int CustomerID { get; set; }
        public int AmountBeforeDiscount { get; set; }
        public float Discount { get; set; }
        public float TotalAmount { get; set; }
        public String DateTime { get; set; }


    }
}