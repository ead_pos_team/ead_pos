﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PracticePart1.Models
{
    [Table("Product")]
    public class ProductDTO
    {
        [Key]
        public int ProductID { get; set; }
        public String Name { get; set; }
        public String Manufacturer { get; set; }
        public String Expiry { get; set; }
        public int Price { get; set; }
        public String MaufacturingDate { get; set; }
    }
}