﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;

using PracticePart1.Models;
using System.Data.Entity;

namespace PracticePart1.Controllers
{
    
    public class CustomerController : Controller
    {

        public ActionResult Reciept()
        {

            return View();
        }
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult RemoteData(string query)
        {
            MyDBService db = new MyDBService();
            List<string> listData = db.Search(query);
            //checking the query parameter sent from view. If it is null we will return null else we will return list based on query.
            if (!string.IsNullOrEmpty(query))
            {
                //Created an array of players. We can fetch this from database as well.
               // string[] arrayData = new string[] { "Fabregas", "Messi", "Ronaldo", "Ronaldinho", "Goetze", "Cazorla", "Henry", "Luiz", "Reus", "Neur", "Podolski" };

                //Using Linq to query the result from an array matching letter entered in textbox.
               // listData = arrayData.Where(q => q.ToLower().Contains(query.ToLower())).ToList();
            
            }

            //Returning the matched list as json data.
            return Json(new { Data = listData });
        }
        public ActionResult Home()
        {
            return View();
        }
        public ActionResult NewCust()
        {
            return View();
        }
        public ActionResult ShowAll()
        {
            return View();
        }
        public ActionResult Login(UserDTO u)
        {
            // this action is for handle post (login)
            if (ModelState.IsValid) // this is check validity
            {
                using (MyDBContext dc = new MyDBContext())
                {
                    var v = dc.User.Where(a => a.Email.Equals(u.Email) && a.Password.Equals(u.Password)).FirstOrDefault();
                    if (v != null)
                    {
                        Session["LogedUserID"] = v.Email.ToString();
                        Session["LogedUserFullname"] = v.Email.ToString();
                        return RedirectToAction("NewCust");
                    }
                }
            }
            return View(u);
        }

        [HttpPost]
        public JsonResult SaveCustomer(CustomerDTO dto)
        {
            MyDBService o = new MyDBService();
            ProductDTO p = new ProductDTO();

            p.Name = "Umar";
            p.Manufacturer = "umar";
            p.Price = 34;
            p.Expiry = "umar";
            p.MaufacturingDate = "umar123";
            o.SaveProduct(p);

            StockDTO st = new StockDTO();
            st.ProductID = 2;
            st.Quantity = 5;
            st.LastUpdated = "20may";
            o.SaveStock(st);

            CustomerRepository repo = new CustomerRepository();
            int cid = repo.SaveCustomer(dto);

            Object obj = new
            {
                CustomerID = cid,
                success = true,
                message = "Successfully Saved"
            };

            return Json(obj);

        }
        [HttpGet]
        public JsonResult GetAllCustomers()
        {
            CustomerRepository repo = new CustomerRepository();
            var data = repo.GetAllCustomers();

            Object obj = new
            {
                Result = data,
                success = true,
            };
            return Json(obj, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Products()
        {
            using (var ctx = new MyDBContext())
            {

                return View(ctx.Product.ToList());
            }



        }

        public ActionResult Create()
        {

            using (var ctx = new MyDBContext())
            {

                return View();
            }
        }
        [HttpPost , ValidateAntiForgeryToken ]
        public ActionResult Create(ProductDTO p)
        {

            using (var ctx = new MyDBContext())
            {

                ctx.Product.Add(p);
                ctx.SaveChanges();
                return RedirectToAction("Products");
            }
        }

        public ActionResult Edit(int id=0)
        {
            var ctx = new MyDBContext();
            return View(ctx.Product.Find(id));

        }

        [HttpPost , ValidateAntiForgeryToken]
        public ActionResult Edit (ProductDTO p)
        {
            var ctx = new MyDBContext();
            ctx.Entry(p).State = EntityState.Modified;
            ctx.SaveChanges();
            return RedirectToAction("Products");
        }

        public ActionResult Delete(int id=0)
        {
            var ctx = new MyDBContext();
            return View(ctx.Product.Find(id));
        }

        [HttpPost , ActionName("Delete")]

        public ActionResult Delete_configuration(int id)
        {
            MyDBContext ctx = new MyDBContext();
            ProductDTO p =ctx.Product.Find(id);
            ctx.Product.Remove(p);
            ctx.SaveChanges();
            return RedirectToAction("Products");
        }
    }
}
